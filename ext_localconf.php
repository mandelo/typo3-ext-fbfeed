<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSconfig/page.typoscript">'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSconfig/user.typoscript">'
);

/*
 * Configure Plugin
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Somedia.' . $_EXTKEY,
    'Feed',
    array(
        'Feed' => 'list'
    )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Somedia.' . $_EXTKEY,
    'Events',
    array(
        'Events' => 'list'
    )
);

/*
 * Register new command controller task (planer)
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][$_EXTKEY] = Somedia\Fbfeed\Command\ImportCommandController::class;

/*
 * Register hook to save Feed Model of feed plugin content element
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = 'Somedia\Fbfeed\Hooks\Backend\FlexFormHook';