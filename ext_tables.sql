CREATE TABLE tx_fbfeed_domain_model_feed (
  uid int(11) unsigned DEFAULT '0' NOT NULL AUTO_INCREMENT,
  pid int(11) DEFAULT '0' NOT NULL,

  content_element_id INT(11) NOT NULL,
  page_id VARCHAR(51) NOT NULL,
  max_posts INT(11),
  post_template VARCHAR(255),
  max_characters INT(11),
  likes TINYINT(1),
  reactions TINYINT(1),
  shares TINYINT(1),
  comments TINYINT(1),
  images TINYINT(1),
  videos TINYINT(1),
  events TINYINT(1),
  status TINYINT(1),
  links TINYINT(1),

  PRIMARY KEY (uid),
  KEY parent (pid)
);

CREATE TABLE tx_fbfeed_domain_model_eventsoverview (
  uid int(11) unsigned DEFAULT '0' NOT NULL AUTO_INCREMENT,
  pid int(11) DEFAULT '0' NOT NULL,

  content_element_id INT(11) NOT NULL,
  page_id VARCHAR(51) NOT NULL,
  max_events INT(11),
  events_state VARCHAR(51) DEFAULT '0',
  show_images TINYINT(1),

  PRIMARY KEY (uid),
  KEY parent (pid)
);

CREATE TABLE tx_fbfeed_domain_model_page (
  uid int(11) unsigned DEFAULT '0' NOT NULL AUTO_INCREMENT,
  pid int(11) DEFAULT '0' NOT NULL,

  page_id VARCHAR(51) NOT NULL,
  id VARCHAR(51) NOT NULL,
  name VARCHAR(255) NOT NULL,
  about VARCHAR(255),
  picture VARCHAR(255),
  cover VARCHAR(255),
  fan_count INT(11),

  PRIMARY KEY (uid),
  KEY parent (pid)
);

CREATE TABLE tx_fbfeed_domain_model_event (
  uid int(11) unsigned DEFAULT '0' NOT NULL AUTO_INCREMENT,
  pid int(11) DEFAULT '0' NOT NULL,

  page_id VARCHAR(51) NOT NULL,
  id VARCHAR(51) NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255),
  place VARCHAR(255),
  cover VARCHAR(255),
  start_time DATETIME,
  end_time DATETIME,
  guest_list_enabled TINYINT(1),
  can_guests_invite TINYINT(1),
  attending_count INT(11),
  declined_count INT(11),
  interested_count INT(11),
  maybe_count INT(11),
  noreply_count INT(11),

  PRIMARY KEY (uid),
  KEY parent (pid)
);

CREATE TABLE tx_fbfeed_domain_model_post (
  uid int(11) unsigned DEFAULT '0' NOT NULL AUTO_INCREMENT,
  pid int(11) DEFAULT '0' NOT NULL,

  page_id VARCHAR(51) NOT NULL,
  id VARCHAR(51) NOT NULL,
  created_time DATETIME NOT NULL,
  type VARCHAR(255) NOT NULL,
  is_hidden TINYINT(1),
  is_published TINYINT(1),
  shares INT(11),
  comments INT(11),
  'like' INT(11),
  love INT(11),
  haha INT(11),
  wow INT(11),
  sad INT(11),
  angry INT(11),
  message VARCHAR(2550),
  picture VARCHAR(255),
  full_picture VARCHAR(255),
  source VARCHAR(255),
  link VARCHAR(255) NOT NULL,
  permalink_url VARCHAR(255) NOT NULL,

  PRIMARY KEY (uid),
  KEY parent (pid)
);