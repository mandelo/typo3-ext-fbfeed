<?php

namespace Somedia\Fbfeed\Domain\Model;

use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Feed
 * @package Somedia\Fbfeed\Domain\Model
 */
class Feed extends AbstractEntity
{
    /**
     * @var integer
     */
    protected $contentElementId;

    /**
     * @var string
     */
    protected $pageId;

    /**
     * @var integer
     */
    protected $maxPosts;

    /**
     * @var string
     */
    protected $postTemplate;

    /**
     * @var integer
     */
    protected $maxCharacters;

    /**
     * @var boolean
     */
    protected $likes;

    /**
     * @var boolean
     */
    protected $reactions;

    /**
     * @var boolean
     */
    protected $shares;

    /**
     * @var boolean
     */
    protected $comments;

    /**
     * @var boolean
     */
    protected $images;

    /**
     * @var boolean
     */
    protected $videos;

    /**
     * @var boolean
     */
    protected $events;

    /**
     * @var boolean
     */
    protected $status;

    /**
     * @var boolean
     */
    protected $links;

    /**
     * Feed constructor.
     * @param int $contentElementId
     * @param string $pageId
     * @param int $maxPosts
     * @param string $postTemplate
     * @param int $maxCharacters
     * @param string $reactions
     * @param bool $likes
     * @param bool $reactions
     * @param bool $shares
     * @param bool $comments
     * @param bool $images
     * @param bool $videos
     * @param bool $events
     * @param bool $status
     * @param bool $links
     */
    public function __construct(
        $contentElementId = 0,
        $pageId = '',
        $maxPosts = 0,
        $postTemplate = '',
        $maxCharacters = 8,
        $likes = true,
        $reactions = true,
        $shares = true,
        $comments = true,
        $images = true,
        $videos = true,
        $events = true,
        $status = true,
        $links = true
    )
    {
        $this->setContentElementId($contentElementId);
        $this->setPageId($pageId);
        $this->setMaxPosts($maxPosts);
        $this->setPostTemplate($postTemplate);
        $this->setMaxCharacters($maxCharacters);
        $this->setLikes($likes);
        $this->setReactions($reactions);
        $this->setShares($shares);
        $this->setComments($comments);
        $this->setImages($images);
        $this->setVideos($videos);
        $this->setEvents($events);
        $this->setStatus($status);
        $this->setLinks($links);
    }

    /**
     * @return int
     */
    public function getContentElementId(): int
    {
        return $this->contentElementId;
    }

    /**
     * @param int $contentElementId
     */
    public function setContentElementId(int $contentElementId)
    {
        $this->contentElementId = $contentElementId;
    }

    /**
     * @return string
     */
    public function getPageId(): string
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     */
    public function setPageId(string $pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return int
     */
    public function getMaxPosts(): int
    {
        return $this->maxPosts;
    }

    /**
     * @param int $maxPosts
     */
    public function setMaxPosts(int $maxPosts)
    {
        $this->maxPosts = $maxPosts;
    }

    /**
     * @return string
     */
    public function getPostTemplate(): string
    {
        return $this->postTemplate;
    }

    /**
     * @param string $postTemplate
     */
    public function setPostTemplate(string $postTemplate)
    {
        $this->postTemplate = $postTemplate;
    }

    /**
     * @return int
     */
    public function getMaxCharacters(): int
    {
        return $this->maxCharacters;
    }

    /**
     * @param int $maxCharacters
     */
    public function setMaxCharacters(int $maxCharacters)
    {
        $this->maxCharacters = $maxCharacters;
    }

    /**
     * @return bool
     */
    public function getReactions(): bool
    {
        return $this->reactions;
    }

    /**
     * @param bool $reactions
     */
    public function setReactions(bool $reactions)
    {
        $this->reactions = $reactions;
    }

    /**
     * @return bool
     */
    public function getComments(): bool
    {
        return $this->comments;
    }

    /**
     * @param bool $comments
     */
    public function setComments(bool $comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return bool
     */
    public function isLikes(): bool
    {
        return $this->likes;
    }

    /**
     * @param bool $likes
     */
    public function setLikes(bool $likes)
    {
        $this->likes = $likes;
    }

    /**
     * @return bool
     */
    public function isShares(): bool
    {
        return $this->shares;
    }

    /**
     * @param bool $shares
     */
    public function setShares(bool $shares)
    {
        $this->shares = $shares;
    }

    /**
     * @return bool
     */
    public function isImages(): bool
    {
        return $this->images;
    }

    /**
     * @param bool $images
     */
    public function setImages(bool $images)
    {
        $this->images = $images;
    }

    /**
     * @return bool
     */
    public function isVideos(): bool
    {
        return $this->videos;
    }

    /**
     * @param bool $videos
     */
    public function setVideos(bool $videos)
    {
        $this->videos = $videos;
    }

    /**
     * @return bool
     */
    public function isEvents(): bool
    {
        return $this->events;
    }

    /**
     * @param bool $events
     */
    public function setEvents(bool $events)
    {
        $this->events = $events;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isLinks(): bool
    {
        return $this->links;
    }

    /**
     * @param bool $links
     */
    public function setLinks(bool $links)
    {
        $this->links = $links;
    }

}