<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Page
 * @package Somedia\Fbfeed\Domain\Model
 */
class Page extends AbstractEntity
{
    /**
     * @var string $id
     */
    protected $pageId;

    /**
     * @var string $id
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string $about
     */
    protected $about;

    /**
     * @var string $picture
     */
    protected $picture;

    /**
     * @var string $cover
     */
    protected $cover;

    /**
     * @var int $fanCount
     */
    protected $fanCount;

    /**
     * Page constructor.
     * @param string $pageId
     * @param string $id
     * @param string $name
     * @param string $about
     * @param string $picture
     * @param string $cover
     * @param int $fanCount
     */
    public function __construct(
        $pageId = '',
        $id = '',
        $name = '',
        $about = '',
        $picture = '',
        $cover = '',
        $fanCount = 0
    )
    {
        $this->setPageId($pageId);
        $this->setId($pageId);
        $this->setName($name);
        $this->setAbout($about);
        $this->setPicture($picture);
        $this->setCover($cover);
        $this->setFanCount($fanCount);
    }

    /**
     * @return string
     */
    public function getPageId(): string
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     */
    public function setPageId(string $pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAbout(): string
    {
        return $this->about;
    }

    /**
     * @param string $about
     */
    public function setAbout(string $about)
    {
        $this->about = $about;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getCover(): string
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     */
    public function setCover(string $cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return int
     */
    public function getFanCount(): int
    {
        return $this->fanCount;
    }

    /**
     * @param int $fanCount
     */
    public function setFanCount(int $fanCount)
    {
        $this->fanCount = $fanCount;
    }
}