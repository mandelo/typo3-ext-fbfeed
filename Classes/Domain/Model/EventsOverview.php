<?php

namespace Somedia\Fbfeed\Domain\Model;

use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class EventsOverview
 * @package Somedia\Fbfeed\Domain\Model
 */
class EventsOverview extends AbstractEntity
{
    /**
     * @var integer
     */
    protected $contentElementId;

    /**
     * @var string
     */
    protected $pageId;

    /**
     * @var integer
     */
    protected $maxEvents;

    /**
     * @var string
     */
    protected $eventsState;

    /**
     * @var boolean
     */
    protected $showImages;

    public function __construct(
        $contentElementId = 0,
        $pageId = '',
        $maxEvents = 0,
        $eventsState = '',
        $showImages = True
    )
    {
        $this->setContentElementId($contentElementId);
        $this->setPageId($pageId);
        $this->setMaxEvents($maxEvents);
        $this->setEventsState($eventsState);
        $this->setShowImages($showImages);
    }

    /**
     * @return int
     */
    public function getContentElementId(): int
    {
        return $this->contentElementId;
    }

    /**
     * @param int $contentElementId
     */
    public function setContentElementId(int $contentElementId)
    {
        $this->contentElementId = $contentElementId;
    }

    /**
     * @return string
     */
    public function getPageId(): string
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     */
    public function setPageId(string $pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return int
     */
    public function getMaxEvents(): int
    {
        return $this->maxEvents;
    }

    /**
     * @param int $maxEvents
     */
    public function setMaxEvents(int $maxEvents)
    {
        $this->maxEvents = $maxEvents;
    }

    /**
     * @return string
     */
    public function getEventsState(): string
    {
        return $this->eventsState;
    }

    /**
     * @param string $eventsState
     */
    public function setEventsState(string $eventsState)
    {
        $this->eventsState = $eventsState;
    }

    /**
     * @return bool
     */
    public function isShowImages(): bool
    {
        return $this->showImages;
    }

    /**
     * @param bool $showImages
     */
    public function setShowImages(bool $showImages)
    {
        $this->showImages = $showImages;
    }
}