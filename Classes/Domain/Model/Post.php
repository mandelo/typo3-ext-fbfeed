<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Model;

use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Post
 * @package Somedia\Fbfeed\Domain\Model
 */
class Post extends AbstractEntity
{

    /**
     * @var string $pageId
     */
    protected $pageId;

    /**
     * @var string $id
     */
    protected $id;

    /**
     * @var string $createdTime
     */
    protected $createdTime;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var bool $isHidden
     */
    protected $isHidden;

    /**
     * @var bool $isPublished
     */
    protected $isPublished;

    /**
     * @var int $shares
     */
    protected $shares;

    /**
     * @var int $comments
     */
    protected $comments;

    /**
     * @var int $like
     */
    protected $like;

    /**
     * @var int $love
     */
    protected $love;

    /**
     * @var int $haha
     */
    protected $haha;

    /**
     * @var int $wow
     */
    protected $wow;

    /**
     * @var int $sad
     */
    protected $sad;

    /**
     * @var int $angry
     */
    protected $angry;

    /**
     * @var string $message
     */
    protected $message;

    /**
     * @var string $picture
     */
    protected $picture;

    /**
     * @var string $fullPicture
     */
    protected $fullPicture;

    /**
     * @var string $source
     */
    protected $source;

    /**
     * @var string $link
     */
    protected $link;

    /**
     * @var string $permalinkUrl
     */
    protected $permalinkUrl;

    /**
     * Post constructor.
     *
     * @param string $pageId
     * @param string $id
     * @param string $createdTime
     * @param string $type
     * @param bool $isHidden
     * @param bool $isPublished
     * @param int $shares
     * @param int $comments
     * @param int $like
     * @param int $love
     * @param int $haha
     * @param int $wow
     * @param int $sad
     * @param int $angry
     * @param string $message
     * @param string $picture
     * @param string $fullPicture
     * @param string $source
     * @param string $link
     * @param string $permalinkUrl
     */
    public function __construct(
        $pageId = '',
        $id = '',
        $createdTime = '',
        $type = '',
        $isHidden = false,
        $isPublished = true,
        $shares = 0,
        $comments = 0,
        $like = 0,
        $love = 0,
        $haha = 0,
        $wow = 0,
        $sad = 0,
        $angry = 0,
        $message = '',
        $picture = '',
        $fullPicture = '',
        $source = '',
        $link = '',
        $permalinkUrl = ''
    )
    {
        $this->setPageId($pageId);
        $this->setId($id);
        $this->setCreatedTime($createdTime);
        $this->setType($type);
        $this->setIsHidden($isHidden);
        $this->setIsPublished($isPublished);
        $this->setShares($shares);
        $this->setComments($comments);
        $this->setLike($like);
        $this->setLove($love);
        $this->setHaha($haha);
        $this->setWow($wow);
        $this->setSad($sad);
        $this->setAngry($angry);
        $this->setMessage($message);
        $this->setPicture($picture);
        $this->setFullPicture($fullPicture);
        $this->setSource($source);
        $this->setLink($link);
        $this->setPermalinkUrl($permalinkUrl);
    }


    /**
     * @return string
     */
    public function getPageId(): string
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     */
    public function setPageId(string $pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCreatedTime(): string
    {
        return $this->createdTime;
    }

    /**
     * @param string $createdTime
     */
    public function setCreatedTime(string $createdTime)
    {
        $this->createdTime = $createdTime;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->isHidden;
    }

    /**
     * @param bool $isHidden
     */
    public function setIsHidden(bool $isHidden)
    {
        $this->isHidden = $isHidden;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool $isPublished
     */
    public function setIsPublished(bool $isPublished)
    {
        $this->isPublished = $isPublished;
    }

    /**
     * @return int
     */
    public function getShares(): int
    {
        return $this->shares;
    }

    /**
     * @param int $shares
     */
    public function setShares(int $shares)
    {
        $this->shares = $shares;
    }

    /**
     * @return int
     */
    public function getComments(): int
    {
        return $this->comments;
    }

    /**
     * @param int $comments
     */
    public function setComments(int $comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return int
     */
    public function getLike(): int
    {
        return $this->like;
    }

    /**
     * @param int $like
     */
    public function setLike(int $like)
    {
        $this->like = $like;
    }

    /**
     * @return int
     */
    public function getLove(): int
    {
        return $this->love;
    }

    /**
     * @param int $love
     */
    public function setLove(int $love)
    {
        $this->love = $love;
    }

    /**
     * @return int
     */
    public function getHaha(): int
    {
        return $this->haha;
    }

    /**
     * @param int $haha
     */
    public function setHaha(int $haha)
    {
        $this->haha = $haha;
    }

    /**
     * @return int
     */
    public function getWow(): int
    {
        return $this->wow;
    }

    /**
     * @param int $wow
     */
    public function setWow(int $wow)
    {
        $this->wow = $wow;
    }

    /**
     * @return int
     */
    public function getSad(): int
    {
        return $this->sad;
    }

    /**
     * @param int $sad
     */
    public function setSad(int $sad)
    {
        $this->sad = $sad;
    }

    /**
     * @return int
     */
    public function getAngry(): int
    {
        return $this->angry;
    }

    /**
     * @param int $angry
     */
    public function setAngry(int $angry)
    {
        $this->angry = $angry;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getFullPicture(): string
    {
        return $this->fullPicture;
    }

    /**
     * @param string $fullPicture
     */
    public function setFullPicture(string $fullPicture)
    {
        $this->fullPicture = $fullPicture;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getPermalinkUrl(): string
    {
        return $this->permalinkUrl;
    }

    /**
     * @param string $permalinkUrl
     */
    public function setPermalinkUrl(string $permalinkUrl)
    {
        $this->permalinkUrl = $permalinkUrl;
    }


}