<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Event
 *
 * @package Somedia\Fbfeed\Domain\Model
 */
class Event extends AbstractEntity
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $pageId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $place;

    /**
     * @var string
     */
    protected $cover;

    /**
     * @var string
     */
    protected $startTime;

    /**
     * @var string
     */
    protected $endTime;

    /**
     * @var boolean
     */
    protected $guestListEnabled;

    /**
     * @var boolean
     */
    protected $canGuestsInvite;

    /**
     * @var integer
     */
    protected $attendingCount;

    /**
     * @var integer
     */
    protected $declinedCount;

    /**
     * @var integer
     */
    protected $interestedCount;

    /**
     * @var integer
     */
    protected $maybeCount;

    /**
     * @var integer
     */
    protected $noreplyCount;

    /**
     * Event constructor.
     * @param string $id
     * @param string $pageId
     * @param string $name
     * @param string $description
     * @param string $place
     * @param string $cover
     * @param string $startTime
     * @param string $endTime
     * @param bool $guestListEnabled
     * @param bool $canGuestsInvite
     * @param int $attendingCount
     * @param int $declinedCount
     * @param int $interestedCount
     * @param int $maybeCount
     * @param int $noreplyCount
     */
    public function __construct(
        $id = '',
        $pageId = '',
        $name = '',
        $description = '',
        $place = '',
        $cover = '',
        $startTime = '',
        $endTime = '',
        $guestListEnabled = false,
        $canGuestsInvite = false,
        $attendingCount = 0,
        $declinedCount = 0,
        $interestedCount = 0,
        $maybeCount = 0,
        $noreplyCount = 0
    )
    {
        $this->setId($id);
        $this->setPageId($pageId);
        $this->setName($name);
        $this->setDescription($description);
        $this->setPlace($place);
        $this->setCover($cover);
        $this->setStartTime($startTime);
        $this->setEndTime($endTime);
        $this->setGuestListEnabled($guestListEnabled);
        $this->setCanGuestsInvite($canGuestsInvite);
        $this->setAttendingCount($attendingCount);
        $this->setDeclinedCount($declinedCount);
        $this->setInterestedCount($interestedCount);
        $this->setMaybeCount($maybeCount);
        $this->setNoreplyCount($noreplyCount);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPageId(): string
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     */
    public function setPageId(string $pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getPlace(): string
    {
        return $this->place;
    }

    /**
     * @param string $place
     */
    public function setPlace(string $place)
    {
        $this->place = $place;
    }

    /**
     * @return string
     */
    public function getCover(): string
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     */
    public function setCover(string $cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     */
    public function setStartTime(string $startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return string
     */
    public function getEndTime(): string
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     */
    public function setEndTime(string $endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return bool
     */
    public function isGuestListEnabled(): bool
    {
        return $this->guestListEnabled;
    }

    /**
     * @param bool $guestListEnabled
     */
    public function setGuestListEnabled(bool $guestListEnabled)
    {
        $this->guestListEnabled = $guestListEnabled;
    }

    /**
     * @return bool
     */
    public function isCanGuestsInvite(): bool
    {
        return $this->canGuestsInvite;
    }

    /**
     * @param bool $canGuestsInvite
     */
    public function setCanGuestsInvite(bool $canGuestsInvite)
    {
        $this->canGuestsInvite = $canGuestsInvite;
    }

    /**
     * @return int
     */
    public function getAttendingCount(): int
    {
        return $this->attendingCount;
    }

    /**
     * @param int $attendingCount
     */
    public function setAttendingCount(int $attendingCount)
    {
        $this->attendingCount = $attendingCount;
    }

    /**
     * @return int
     */
    public function getDeclinedCount(): int
    {
        return $this->declinedCount;
    }

    /**
     * @param int $declinedCount
     */
    public function setDeclinedCount(int $declinedCount)
    {
        $this->declinedCount = $declinedCount;
    }

    /**
     * @return int
     */
    public function getInterestedCount(): int
    {
        return $this->interestedCount;
    }

    /**
     * @param int $interestedCount
     */
    public function setInterestedCount(int $interestedCount)
    {
        $this->interestedCount = $interestedCount;
    }

    /**
     * @return int
     */
    public function getMaybeCount(): int
    {
        return $this->maybeCount;
    }

    /**
     * @param int $maybeCount
     */
    public function setMaybeCount(int $maybeCount)
    {
        $this->maybeCount = $maybeCount;
    }

    /**
     * @return int
     */
    public function getNoreplyCount(): int
    {
        return $this->noreplyCount;
    }

    /**
     * @param int $noreplyCount
     */
    public function setNoreplyCount(int $noreplyCount)
    {
        $this->noreplyCount = $noreplyCount;
    }

}