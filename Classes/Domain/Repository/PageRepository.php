<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Repository;

use Somedia\Fbfeed\Domain\Model\Page;
use \TYPO3\CMS\Extbase\Persistence\Repository;

class PageRepository extends Repository
{
    public function addOrUpdate(Page $tempPage)
    {
        /** @var Page $page */
        $page = $this->findOneByPageId($tempPage->getPageId());

        if (is_null($page)) {
            $this->add($tempPage);
        } else {
            $page->setId($tempPage->getId());
            $page->setName($tempPage->getName());
            $page->setAbout($tempPage->getAbout());
            $page->setPicture($tempPage->getPicture());
            $page->setCover($tempPage->getCover());
            $page->setFanCount($tempPage->getFanCount());

            $this->update($page);
        }
    }

    /**
     * Persist all added and modified objects
     * @return void
     */
    public function _persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}