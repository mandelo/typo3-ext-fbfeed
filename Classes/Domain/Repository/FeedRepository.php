<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Repository;

use Somedia\Fbfeed\Domain\Model\Feed;
use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class FeedRepository
 * @package Somedia\Fbfeed\Domain\Repository
 */
class FeedRepository extends Repository
{
    /**
     * @param Feed $tempFeed
     */
    public function addOrUpdate(Feed $tempFeed)
    {

        /** @var Feed $feed */
        $feed = $this->findOneByContentElementId($tempFeed->getContentElementId());

        if (is_null($feed)) {
            $this->add($tempFeed);
        } else {
            $feed->setContentElementId($tempFeed->getContentElementId());
            $feed->setPageId($tempFeed->getPageId());
            $feed->setMaxPosts($tempFeed->getMaxPosts());
            $feed->setPostTemplate($tempFeed->getPostTemplate());
            $feed->setMaxCharacters($tempFeed->getMaxCharacters());
            $feed->setLikes($tempFeed->isLikes());
            $feed->setReactions($tempFeed->getReactions());
            $feed->setShares($tempFeed->isShares());
            $feed->setComments($tempFeed->getComments());
            $feed->setImages($tempFeed->isImages());
            $feed->setVideos($tempFeed->isVideos());
            $feed->setEvents($tempFeed->isEvents());
            $feed->setStatus($tempFeed->isStatus());
            $feed->setLinks($tempFeed->isLinks());

            $this->update($feed);
        }

        $this->_persistAll();
    }

    /**
     * Find all feeds and ignore PID
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllIgnorePid()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        return $query->execute();
    }

    public function findOneIgnorePid($contentElementId)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching($query->equals('content_element_id', $contentElementId));
        return $query->execute()[0];
    }

    /**
     * Persist all added and modified objects
     * @return void
     */
    public function _persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}