<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Repository;

use Somedia\Fbfeed\Domain\Model\EventsOverview;
use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class EventsOverviewRepository
 *
 * @package Somedia\Fbfeed\Domain\Repository
 */
class EventsOverviewRepository extends Repository
{
    /**
     * Add or update model.
     *
     * @param EventsOverview $tempEvents
     */
    public function addOrUpdate(EventsOverview $tempEvents)
    {
        /** @var EventsOverview $eventsOverview */
        $eventsOverview = $this->findOneByContentElementId($tempEvents->getContentElementId());

        if (is_null($eventsOverview)) {
            $this->add($tempEvents);
        } else {
            $eventsOverview->setContentElementId($tempEvents->getContentElementId());
            $eventsOverview->setPageId($tempEvents->getPageId());
            $eventsOverview->setMaxEvents($tempEvents->getMaxEvents());
            $eventsOverview->setEventsState($tempEvents->getEventsState());
            $eventsOverview->setShowImages($tempEvents->isShowImages());

            $this->update($eventsOverview);
        }

        $this->_persistAll();
    }

    /**
     * Persist all added and modified objects
     * @return void
     */
    public function _persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}