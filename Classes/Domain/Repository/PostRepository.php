<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Repository;

use \Somedia\Fbfeed\Domain\Model\Feed;
use Somedia\Fbfeed\Domain\Model\Post;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class PostRepository
 * @package Somedia\Fbfeed\Domain\Repository
 */
class PostRepository extends Repository
{
    /**
     * @param Post $tempPost
     */
    public function addOrUpdate(Post $tempPost)
    {
        /** @var Post $post */
        $post = $this->findOneById($tempPost->getId());

        if (is_null($post)) {
            $this->add($tempPost);
        } else {
            $post->setPageId($tempPost->getPageId());
            $post->setId($tempPost->getId());
            $post->setCreatedTime($tempPost->getCreatedTime());
            $post->setType($tempPost->getType());
            $post->setIsHidden($tempPost->isHidden());
            $post->setIsPublished($tempPost->isPublished());
            $post->setShares($tempPost->getShares());
            $post->setComments($tempPost->getComments());
            $post->setLike($tempPost->getLike());
            $post->setLove($tempPost->getLove());
            $post->setHaha($tempPost->getHaha());
            $post->setWow($tempPost->getWow());
            $post->setSad($tempPost->getSad());
            $post->setAngry($tempPost->getAngry());
            $post->setMessage($tempPost->getMessage());
            $post->setPicture($tempPost->getPicture());
            $post->setFullPicture($tempPost->getFullPicture());
            $post->setSource($tempPost->getSource());
            $post->setLink($tempPost->getLink());
            $post->setPermalinkUrl($tempPost->getPermalinkUrl());

            $this->update($post);
        }
    }

    public function findAllEvents(Feed $feed)
    {
        $constraints = array();

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        $constraints[] = $query->equals('isHidden', false);
        $constraints[] = $query->equals('isPublished', true);
        $constraints[] = $query->equals('pageId', $feed->getPageId());


    }

    /**
     * @param \Somedia\Fbfeed\Domain\Model\Feed
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllByFeed(Feed $feed)
    {
        $constraints = array();

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        $constraints[] = $query->equals('isHidden', false);
        $constraints[] = $query->equals('isPublished', true);
        $constraints[] = $query->equals('pageId', $feed->getPageId());

        if ($feed->isImages()) {
            $types[] = $query->equals('type', 'photo');
        }
        if ($feed->isVideos()) {
            $types[] = $query->equals('type', 'video');
        }
        if ($feed->isStatus()) {
            $types[] = $query->equals('type', 'status');
        }
        if ($feed->isLinks()) {
            $types[] = $query->equals('type', 'link');
        }
        if ($feed->isEvents()) {
            $types[] = $query->equals('type', 'event');
        }

        $constraints[] = $query->logicalOr($types);

        $query->matching(
            $query->logicalAnd($constraints)
        );

        $query->setOrderings(
            array(
                'createdTime' => QueryInterface::ORDER_DESCENDING
            )
        );
        $query->setLimit(
            $feed->getMaxPosts()
        );

        return $query->execute();
    }

    /**
     * Persist all added and modified objects
     * @return void
     */
    public function _persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}