<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Domain\Repository;

use Somedia\Fbfeed\Domain\Model\Event;
use Somedia\Fbfeed\Domain\Model\EventsOverview;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use \TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class EventRepository
 * @package Somedia\Fbfeed\Domain\Repository
 */
class EventRepository extends Repository
{
    /**
     * Add or update model.
     *
     * @param Event $tempEvent
     */
    public function addOrUpdate(Event $tempEvent)
    {
        /** @var Event $event */
        $event = $this->findOneById($tempEvent->getId());

        if (is_null($event)) {
            $this->add($tempEvent);
        } else {
            $event->setId($tempEvent->getId());
            $event->setPageId($tempEvent->getPageId());
            $event->setName($tempEvent->getName());
            $event->setPlace($tempEvent->getPlace());
            $event->setCover($tempEvent->getCover());
            $event->setStartTime($tempEvent->getStartTime());
            $event->setEndTime($tempEvent->getEndTime());
            $event->setGuestListEnabled($tempEvent->isGuestListEnabled());
            $event->setCanGuestsInvite($tempEvent->isCanGuestsInvite());
            $event->setAttendingCount($tempEvent->getAttendingCount());
            $event->setDeclinedCount($tempEvent->getDeclinedCount());
            $event->setInterestedCount($tempEvent->getInterestedCount());
            $event->setMaybeCount($tempEvent->getMaybeCount());
            $event->setNoreplyCount($tempEvent->getNoreplyCount());

            $this->update($event);
        }

        $this->_persistAll();
    }

    /**
     * @param \Somedia\Fbfeed\Domain\Model\EventsOverview
     */
    public function findAllByEventsOverview(EventsOverview $eventsOverview)
    {
        $constraints = array();

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        $constraints[] = $query->equals('pageId', $eventsOverview->getPageId());

        $query->matching(
            $query->logicalAnd($constraints)
        );

        $query->setOrderings(
            array(
                'startTime' => QueryInterface::ORDER_DESCENDING
            )
        );
        $query->setLimit(
            $eventsOverview->getMaxEvents()
        );

        return $query->execute();
    }

    /**
     * Persist all added and modified objects
     * @return void
     */
    public function _persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}