<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Service;

use Somedia\Fbfeed\Adapter\FacebookAdapter;
use Somedia\Fbfeed\Domain\Model\EventsOverview;
use Somedia\Fbfeed\Domain\Model\Feed;

use Somedia\Fbfeed\Domain\Model\Event;
use Somedia\Fbfeed\Domain\Model\Page;
use Somedia\Fbfeed\Domain\Model\Post;

use Somedia\Fbfeed\Domain\Repository\EventRepository;
use Somedia\Fbfeed\Domain\Repository\EventsOverviewRepository;
use Somedia\Fbfeed\Domain\Repository\FeedRepository;
use Somedia\Fbfeed\Domain\Repository\PageRepository;
use Somedia\Fbfeed\Domain\Repository\PostRepository;
use \TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class ImportService
 * @package Somedia\Fbfeed\Service
 */
class ImportService implements SingletonInterface
{
    /** @var ObjectManager $extbaseObjectManager */
    protected $extbaseObjectManager;

    /** @var  FacebookAdapter $facebookAdapter */
    protected $facebookAdapter;

    /** @var  EventRepository $eventRepository */
    protected $eventRepository;

    /** @var  PageRepository $pageRepository */
    protected $pageRepository;

    /** @var  PostRepository $postRepository */
    protected $postRepository;

    /** @var  FeedRepository $feedRepository */
    protected $feedRepository;

    /** @var  EventsOverviewRepository $eventsOverviewRepository */
    protected $eventsOverviewRepository;

    public function __construct()
    {
        $this->extbaseObjectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        $this->facebookAdapter = $this->extbaseObjectManager->get('Somedia\Fbfeed\Adapter\FacebookAdapter');
        $this->eventRepository = $this->extbaseObjectManager->get('Somedia\Fbfeed\Domain\Repository\EventRepository');
        $this->pageRepository = $this->extbaseObjectManager->get('Somedia\Fbfeed\Domain\Repository\PageRepository');
        $this->postRepository = $this->extbaseObjectManager->get('Somedia\Fbfeed\Domain\Repository\PostRepository');
        $this->feedRepository = $this->extbaseObjectManager->get('Somedia\Fbfeed\Domain\Repository\FeedRepository');
        $this->eventsOverviewRepository = $this->extbaseObjectManager->get('Somedia\Fbfeed\Domain\Repository\EventsOverviewRepository');
    }

    /**
     * Imports just everything from the FB Graph API.
     */
    public function importAll()
    {
        if ($this->facebookAdapter->accessToken === false) {
            echo "access token not valid";
            return false;
        }

        /* Import all feeds */
        $this->importAllFeeds();

        /* Import all pages */
        $this->importAllPages();

        /* Import posts of all pages */
        $this->importAllPosts();

        /* Import all events */
        $this->importAllEvents();

        return true;
    }

    /**
     * Imports all feeds
     */
    public function importAllFeeds()
    {
        $feeds = $this->feedRepository->findAllIgnorePid();

        /** @var Feed $feed */
        foreach ($feeds as $feed) {
            $this->importByFeed($feed);
        }
    }

    /**
     * Import all pages
     */
    public function importAllPages()
    {
        $pages = $this->pageRepository->findAll();

        /** @var Page $page */
        foreach ($pages as $page) {
            $this->importPageById($page->getPageId());
        }
    }

    /**
     * Import all posts
     */
    public function importAllPosts()
    {
        $pages = $this->pageRepository->findAll();

        /** @var Page $page */
        foreach ($pages as $page) {
            $this->importPostsByPage($page);
        }
    }

    /**
     * Import all eventsOverview objects
     */
    public function importAllEvents()
    {
        $eventsOverviews = $this->eventsOverviewRepository->findAll();

        /** @var EventsOverview $eventsOverview */
        foreach ($eventsOverviews as $eventsOverview) {
            $this->importEventsByPageId($eventsOverview->getPageId());
        }
    }

    /**
     * Import by feed
     *
     * @param Feed $feed
     */
    public function importByFeed(Feed $feed)
    {
        $page = $this->pageRepository->findOneByPageId($feed->getPageId());
        if (is_null($page)) {

            /** @var \Somedia\Fbfeed\Domain\Model\Page $page */
            $page = GeneralUtility::makeInstance(Page::class, $feed->getPageId());

            $this->pageRepository->add($page);
            $this->pageRepository->_persistAll();
        }
        $this->importPageById($page->getPageId());
    }

    public function importByEventsOverview(EventsOverview $eventsOverview)
    {
        $this->importPageById($eventsOverview->getPageId());
        $this->importEventsByPageId($eventsOverview->getPageId());
    }

    /**
     * Import by page id.
     *
     * @param string $pageId
     */
    public function importEventsByPageId(string $pageId)
    {
        $eventIds = $this->facebookAdapter->getEventsByPageId($pageId);

        if (is_array($eventIds)) {
            foreach ($eventIds['data'] as $eventId) {
                $this->importEventById($pageId, $eventId['id']);
            }
        }
    }

    /**
     * Import Event by its ID
     *
     * @param $pageId
     * @param string $id
     */
    private function importEventById($pageId, string $id)
    {
        /** @var Event $event */
        $eventData = $this->facebookAdapter->getEventById($id);

        /** @var \Somedia\Fbfeed\Domain\Model\Event $event */
        $event = GeneralUtility::makeInstance(Event::class);

        if (!isset($eventData['place']['name'])) {
            $eventData['place']['name'] = '';
        }

        $event->setId($id);
        $event->setPageId($pageId);
        $event->setName($eventData['name']);
        $event->setPlace($eventData['place']['name']);
        $event->setCover($eventData['cover']['source']);
        $event->setStartTime($eventData['start_time']);
        $event->setEndTime($eventData['end_time']);
        $event->setGuestListEnabled($eventData['guest_list_enabled']);
        $event->setCanGuestsInvite($eventData['can_guests_invite']);
        $event->setAttendingCount($eventData['attending']['summary']['count']);
        $event->setDeclinedCount($eventData['declined']['summary']['count']);
        $event->setInterestedCount($eventData['interested']['summary']['count']);
        $event->setMaybeCount($eventData['maybe']['summary']['count']);
        $event->setNoreplyCount($eventData['noreply']['summary']['count']);

        $this->eventRepository->addOrUpdate($event);
    }

    /**
     * Import a page by its Id.
     *
     * @param string $pageId
     */
    public function importPageById(string $pageId)
    {
        $pageData = $this->facebookAdapter->getPageById($pageId);

        /** @var Page $page */
        $page = GeneralUtility::makeInstance(Page::class, $pageId);

        $page->setPageId($pageId);
        $page->setId($pageData['id']);
        $page->setName($pageData['name']);
        $page->setAbout($pageData['about']);
        $page->setPicture($pageData['picture']['data']['url']);
        $page->setCover($pageData['cover']['source']);
        $page->setFanCount($pageData['fan_count']);

        $this->pageRepository->addOrUpdate($page);
    }

    /**
     * Import posts of a page object.
     *
     * @param Page $page
     */
    public function importPostsByPage(Page $page)
    {
        $postsData = $this->facebookAdapter->getPostsByPageId($page->getPageId());

        // array keys which have a data summary (just move on in code to understand)
        $summaryFields = array('comments', 'like', 'love', 'haha', 'wow', 'sad', 'angry');

        if ($postsData !== false) {
            foreach ($postsData as $postData) {

                // Since we call the methods "dynamically", they all need to be in the first level of the array $postData
                if (is_null($postData['shares']['count'])) {
                    $postData['shares'] = $postData['shares']['count'];
                } else {
                    $postData['shares'] = 0;
                }

                // This is exactly the same as the above line, just for all the fields referenced in the array $summaryFields
                foreach ($summaryFields as $summaryField) {
                    $postData[$summaryField] = $postData[$summaryField]['summary']['total_count'];
                }

                // Check if a post object already exists in database

                /** @var \Somedia\Fbfeed\Domain\Model\Post $post */
                $post = $this->postRepository->findOneById($postData['id']);
                if (is_null($post)) {
                    $post = GeneralUtility::makeInstance(Post::class);
                }

                // The feedId obviously doesn't come from the Facebook API so we set it manually
                $post->setPageId($page->getPageId());

                // Set all values from facebook API to post model by its array key
                foreach ($postData as $key => $value) {
                    if (!is_null($value)) {
                        $key = 'set' . GeneralUtility::underscoredToUpperCamelCase($key);
                        if (method_exists($post, $key)) {
                            call_user_func(array($post, $key), $value);
                        }
                    }
                }

                $this->postRepository->addOrUpdate($post);
            }
        }
    }
}