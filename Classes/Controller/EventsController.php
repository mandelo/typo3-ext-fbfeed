<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class EventsController
 * @package Somedia\Fbfeed\Controller
 */
class EventsController extends ActionController
{
    /**
     * @var \Somedia\Fbfeed\Domain\Repository\EventRepository
     * @inject
     */
    private $eventRepository;

    /**
     * @var \Somedia\Fbfeed\Domain\Repository\EventsOverviewRepository
     * @inject
     */
    private $eventsOverviewRepository;

    /**
     * @var \Somedia\Fbfeed\Domain\Repository\PageRepository
     * @inject
     */
    private $pageRepository;

    public function isStaticTemplateIncluded()
    {
        $extName = strtolower($this->extensionName);
        $pluginName = strtolower($this->getControllerContext()->getRequest()->getPluginName());

        $configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
        $settings = $configurationManager->getConfiguration(
            \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
            $extName,
            $extName . '_' . $pluginName
        );

        if (isset($settings['plugin.']['tx_fbfeed.'])) {
            return true;
        } else {
            return false;
        }
    }

    public function listAction()
    {

        // Get UID of the content element object called
        $eventsId = $this->configurationManager->getContentObject()->data['uid'];

        if ($this->isStaticTemplateIncluded() === false){
            $this->view->assignMultiple(array(
                'message_type' => 'error',
                'message_content' => 'Bitte noch statisches TypoScript Template auf Seite einbinden.',
                'id' => $eventsId
            ));
        } else {
            // We need this for a potential error message (we want the user to know which content element caused the error)
            $this->view->assign('id', $eventsId);

            /** @var \Somedia\Fbfeed\Domain\Model\EventsOverview $eventsOverview */
            $eventsOverview = $this->eventsOverviewRepository->findOneByContentElementId($eventsId);

            // If there's a model of the content element object, get all posts of it
            if (!is_null($eventsOverview)) {
                $events = $this->eventRepository->findAllByEventsOverview($eventsOverview)->toArray();

                /** @var \Somedia\Fbfeed\Domain\Model\Page $page */
                $page = $this->pageRepository->findOneByPageId($eventsOverview->getPageId());

                // Assign values to view
                $this->view->assignMultiple(array(
                    'events' => $events,
                    'eventsOverview' => $eventsOverview,
                    'page' => $page
                ));

            } else {
                $this->view->assignMultiple(array(
                    'message_type' => 'error',
                    'message_content' => 'Bitte Inhaltselement im Typo3 Backend editieren und neu abspeichern.',
                    'id' => $eventsId
                ));
            }
        }

        return $this->response->getContent();
    }
}