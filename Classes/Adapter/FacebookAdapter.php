<?php
/**
 * This file is part of the TYPO3 CMS project.
 *
 *  It is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License, either version 2
 *  of the License, or any later version.
 *
 *  For the full copyright and license information, please read the
 *  LICENSE.txt file that was distributed with this source code.
 *
 *  The TYPO3 project - inspiring people to share!
 */

namespace Somedia\Fbfeed\Adapter;

use \Somedia\Fbfeed\Helper\ExtConf;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class FacebookAdapter
 * @package \Somedia\Fbfeed\Adapter
 */
class FacebookAdapter
{
    /**
     * @var \TYPO3\CMS\Core\Http\RequestFactory
     * @inject
     */
    private $requestFactory;

    /**
     * @var \Somedia\Fbfeed\Helper\ExtConf
     * @inject
     */
    private $extConf;

    /**
     * @var string $accessToken
     */
    public $accessToken;

    /**
     * FacebookAdapter constructor.
     */
    public function __construct()
    {
        $this->requestFactory = GeneralUtility::makeInstance(RequestFactory::class);
        $this->extConf = GeneralUtility::makeInstance(ExtConf::class);
        $this->accessToken = $this->claimAccessToken();
    }

    /**
     * @param $path
     * @param null $params
     * @return string
     */
    private function buildUrl($path, $params = null)
    {
        $url = $this->extConf->config['facebook']['api']['url'] . $path . "?";
        if ($params != null || count($params) > 0) {
            $url = $url . http_build_query($params);
        }
        return $url;
    }

    /**
     * @param $url
     * @return bool|mixed
     */
    private function makeHttpRequest($url)
    {
        $response = $this->requestFactory->request($url, 'GET', ['timeout' => 2000, 'http_errors' => false]);

        $result = $response->getBody()->getContents();

        // make it an array if it's json
        if (is_object(json_decode($result))) {
            $result = json_decode($result, True);
        }

        return $result;
    }

    /**
     * @return mixed
     */
    private function claimAccessToken()
    {
        $params = array(
            'client_id' => $this->extConf->config['facebook']['api']['id'],
            'client_secret' => $this->extConf->config['facebook']['api']['secret'],
            'grant_type' => 'client_credentials'
        );
        $token = $this->buildUrl('/oauth/access_token', $params);
        $tokenReq = $this->makeHttpRequest($token);
        if (isset($tokenReq['error'])) {
            return false;
        } else {
            return $tokenReq['access_token'];
        }
    }

    /**
     * @param $url
     * @return bool|mixed
     */
    private function makeApiRequest($url)
    {
        $url = $url . '&access_token=' . $this->accessToken;
        return $this->makeHttpRequest($url);
    }

    /**
     * Returns all specified fields of a page by its id
     *
     * @param string $id
     * @return mixed
     */
    public function getPageById(string $id)
    {
        $fields = array(
            'id',
            'name',
            'about',
            'picture',
            'cover',
            'fan_count'
        );

        $params = array(
            'fields' => implode(',', $fields)
        );

        $url = $this->buildUrl('/' . $id, $params);
        $req = $this->makeApiRequest($url);
        return $req;
    }

    /**
     * Returns all specified fields of an event by its id
     *
     * @param string $id
     * @return bool|mixed
     */
    public function getEventById(string $id)
    {
        $fields = array(
            'id',
            'name',
            'description',
            'place',
            'cover',
            'start_time',
            'end_time',
            'guest_list_enabled',
            'can_guests_invite',
            'attending.limit(0).summary(1)',
            'declined.limit(0).summary(1)',
            'interested.limit(0).summary(1)',
            'maybe.limit(0).summary(1)',
            'noreply.limit(0).summary(1)'
        );

        $params = array(
            'fields' => implode(',', $fields)
        );

        return $this->makeApiRequest($this->buildUrl('/' . $id, $params));
    }

    public function getEventsByPageId(string $id)
    {
        $fields = array();

        $params = array(
            'fields' => implode(',', $fields)
        );

        $url = $this->buildUrl('/' . $id . '/events', $params);

        return $this->makeApiRequest($url);
    }

    /**
     * Returns all specified fields of a all posts of a page by its id
     *
     * @param string $id
     * @return mixed
     */
    public function getPostsByPageId(string $id)
    {
        $fields = array(
            'id',
            'created_time',
            'type',
            'is_hidden',
            'is_published',
            'shares',
            'comments.limit(0).summary(1)',
            'reactions.type(LIKE).limit(0).summary(1).as(like)',
            'reactions.type(LOVE).limit(0).summary(1).as(love)',
            'reactions.type(HAHA).limit(0).summary(1).as(haha)',
            'reactions.type(WOW).limit(0).summary(1).as(wow)',
            'reactions.type(SAD).limit(0).summary(1).as(sad)',
            'reactions.type(ANGRY).limit(0).summary(1).as(angry)',
            'message',
            'picture',
            'full_picture',
            'source',
            'link',
            'permalink_url',
        );

        $params = array(
            'fields' => implode(',', $fields)
        );

        $url = $this->buildUrl('/' . $id . '/posts', $params);

        $result = $this->makeApiRequest($url);

        if (isset($result['error'])) {
            return false;
        } else {
            return $result['data'];
        }
    }

    public function __call($method, $arguments)
    {
        if ($this->accessToken === false) {
            return false;
        } else {

        }
    }
}