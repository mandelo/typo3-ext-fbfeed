<?php

namespace Somedia\Fbfeed\Hooks\Backend;

use Somedia\Fbfeed\Domain\Model\EventsOverview;
use Somedia\Fbfeed\Domain\Model\Feed;
use Somedia\Fbfeed\Service\ImportService;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class FlexFormHook
 * @package Somedia\Fbfeed\Hooks\Backend
 */
class FlexFormHook
{
    /**
     * @var $extbaseObjectManager \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    private $extbaseObjectManager;

    /**
     * @var array $models
     */
    private $models = [
        'fbfeed_feed' => [
            'model' => Feed::class,
            'repository' => 'Somedia\Fbfeed\Domain\Repository\FeedRepository',
            'update' => 'importByFeed'
        ],
        'fbfeed_events' => [
            'model' => EventsOverview::class,
            'repository' => 'Somedia\Fbfeed\Domain\Repository\EventsOverviewRepository',
            'update' => 'importByEventsOverview'
        ]
    ];

    /**
     * FlexFormHook constructor.
     */
    public function __construct()
    {
        $this->extbaseObjectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
    }

    /**
     * Hook which triggers every time a content element is saved
     *
     * @param $status
     * @param $table
     * @param $id
     * @param $fieldArray
     * @param $obj
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $obj)
    {
        if ($status === 'new') {
            $contentElementId = $obj->substNEWwithIDs[$id];
            $activity = 'Creating';
        } else {
            $contentElementId = $id;
            $activity = 'Updating';
        }

        if ($table === 'tt_content') {
            if (isset($fieldArray['pi_flexform'])) {
                $record = $obj->checkValue_currentRecord;
                if (isset($this->models[$record['list_type']])) {
                    if (is_string($fieldArray['pi_flexform'])) {
                        try {
                            $this->updateModel($status, $contentElementId, $record['list_type'], $fieldArray['pi_flexform']);
                            $success = true;
                        } catch (Exception $e) {
                            $success = false;
                        }
                    }
                }
            }
        }

        if ($success === false) {

            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

            $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
                'Unfortunately there was an error when updating the model of this content element.',
                $activity . ' model failed', // [optional] the header
                \TYPO3\CMS\Core\Messaging\FlashMessage::WARNING,
                false
            );

            $flashMessageService = $objectManager->get(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
            $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
            $messageQueue->addMessage($message);
        }

        return;
    }

    /**
     * @param int $numFields
     * @param $number
     * @return array
     */
    private function booleanArrayByNumber($number, $numFields = 4)
    {
        $result = [];
        for ($i = 0; $i < $numFields; $i++) {
            if (($number >> $i) & 1) {
                $result[$i] = true;
            } else {
                $result[$i] = false;
            }
        }
        return $result;
    }

    /**
     * This function uses recursion, that's why we set optionally set an array "results".
     * With this function we enumerate the values of a associative array.
     *
     * @param $array
     * @param array $results
     * @return array
     */
    private function enumAsocArrayValues($array, $results = [])
    {
        foreach ($array as $key => $item) {
            if (isset($item['vDEF'])) {
                $results[$key] = $item['vDEF'];
            } elseif (isset($item['lDEF'])) {
                $results = $this->enumAsocArrayValues($item['lDEF'], $results);
            } else {
                $results = $this->enumAsocArrayValues($item, $results);
            }
        }

        return $results;
    }

    /**
     * This is a function to update a model according to it's flexform configuration.
     * It's global/dynamic and is meant to work for every model.
     *
     * @param $status
     * @param $contentElementId
     * @param $listType
     * @param $flexform
     */
    private function updateModel($status, $contentElementId, $listType, $flexform)
    {
        // Fields which have an own model variable and are boolean
        $booleanFields = [
            'types' => ['images', 'videos', 'events', 'status', 'links'],
            'fields' => ['likes', 'reactions', 'shares', 'comments']
        ];

        // Fields that should be rewritten to insert them into the DB
        $rewriteRules = [
            'show_images' => ['hide' => false, 'show' => true]
        ];

        $repository = $this->extbaseObjectManager->get($this->models[$listType]['repository']);

        $model = GeneralUtility::makeInstance($this->models[$listType]['model']);
        $data = $this->enumAsocArrayValues(GeneralUtility::xml2array($flexform));

        $data['content_element_id'] = $contentElementId;

        // Here we convert the checkbox field booleans to separate model values
        foreach ($data as $key => $item) {
            if (isset($booleanFields[$key])) {
                $result = $this->booleanArrayByNumber($item, count($booleanFields[$key]));
                foreach ($booleanFields[$key] as $index => $field) {
                    $data[$field] = $result[$index];
                }
            }
        }

        // Here we handle the rewrite rules defined above...
        foreach ($data as $key => $value) {
            if (isset($rewriteRules[$key])) {
                $data[$key] = $rewriteRules[$key][$value];
            }
        }

        // Set all values from facebook API to post model by its array key
        foreach ($data as $key => $value) {
            if (!is_null($value)) {
                $key = 'set' . GeneralUtility::underscoredToUpperCamelCase($key);
                if (method_exists($model, $key)) {
                    call_user_func(array($model, $key), $value);
                }
            }
        }

        $repository->addOrUpdate($model);

        // Directly update data with Facebook API
        $importService = $this->extbaseObjectManager->get(ImportService::class);
        call_user_func(array($importService, $this->models[$listType]['update']), $model);
    }
}