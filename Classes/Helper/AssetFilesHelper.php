<?php

namespace Somedia\Fbfeed\Helper;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\PathUtility;

class AssetFilesHelper
{
    public function getWebPath($path)
    {
        if (substr($path, 0, 4) == 'EXT:') {
            $path = substr($path, 4);
        }

        return PathUtility::getAbsoluteWebPath('/typo3conf/ext/' . $path);
    }

    public function getFileHeader($path)
    {
        $path = $this->getWebPath($path);
        $fileExtension = pathinfo($path, PATHINFO_EXTENSION);

        if ($fileExtension == 'css') {
            $header = $this->getCssHeader($path);
        } elseif ($fileExtension == 'js') {
            $header = $this->getJsHeader($path);
        } else {
            $header = False;
        }

        return $header;
    }

    public function getCssHeader($filePath)
    {
        return "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$filePath."\">";
    }

    public function getJsHeader($filePath)
    {
        return "<script type=\"text/javascript\" src=\"".$filePath."\"></script>";
    }
}