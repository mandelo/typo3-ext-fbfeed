<?php

namespace Somedia\Fbfeed\Helper;

class ExtConf
{
    private $extKey = 'fbfeed';
    public $config;

    public function __construct()
    {
        $this->parseExtensionConfiguration();
    }

    /**
     * Removes the dot from the array keys :)
     * Also, it's working recursive!
     *
     * @param $array array
     * @return array
     */
    private function fixKeys($array)
    {
        foreach ($array as $key => $value) {
            if (substr($key, -1) == '.') {
                $newKey = substr($key, 0, -1);
                $array[$newKey] = $array[$key];
                unset($array[$key]);
                if (is_array($array[$newKey])) {
                    $array[$newKey] = $this->fixKeys($array[$newKey]);
                }
            }
        }
        return $array;
    }

    /**
     * Parse the extension configuration file to a beautiful PHP array
     */
    public function parseExtensionConfiguration()
    {
        $this->config = reset($this->fixKeys(unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$this->extKey])));
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->extKey;
    }
}