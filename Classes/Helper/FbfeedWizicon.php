<?php

class FbfeedWizicon
{

    /**
     * Processing the wizard items array
     *
     * @param array $wizardItems The wizard items
     * @return array Modified array with wizard items
     */
    function proc($wizardItems)
    {

        $extKey = 'fbfeed';

        $extPath = \TYPO3\CMS\Core\Utility\PathUtility::getAbsoluteWebPath(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extKey));

        $wizardItems['plugins_tx_' . $extKey . '_feed'] = array(
            'icon' => $extPath . 'Resources/Public/Icons/facebook.png',
            'title' => 'Facebook Posts Feed',
            'description' => 'Stream of facebook posts.',
            'params' => '&defVals[tt_content][CType]=list&defVals[tt_content][list_type]='.$extKey.'_feed'
        );

        $wizardItems['plugins_tx_' . $extKey . '_events'] = array(
            'icon' => $extPath . 'Resources/Public/Icons/facebook.png',
            'title' => 'Facebook Events',
            'description' => 'Events of a Facebook page.',
            'params' => '&defVals[tt_content][CType]=list&defVals[tt_content][list_type]='.$extKey.'_events'
        );

        return $wizardItems;
    }
}