<?php

namespace Somedia\Fbfeed\Command;

use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

/**
 * Class ImportCommandController
 * @package \Somedia\Fbfeed\Command
 */
class ImportCommandController extends CommandController
{
    /**
     * @var \Somedia\Fbfeed\Service\ImportService
     * @inject
     */
    protected $importService;

    /**
     * Import Command
     */
    public function importCommand()
    {
        $this->importService->importAll();
    }
}