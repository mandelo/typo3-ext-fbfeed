<?php
defined('TYPO3_MODE') or die();

/*
 * Register new static file entry for typoscript of extension
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'EXT: Fbfeed'
);

/*
 * Add plugins to "new element" wizard (when choosing the content element in the BE)
 */
$TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']['FbfeedWizicon'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Classes/Helper/FbfeedWizicon.php';

/*
 * PLUGIN EVENTS
 * Function is just for clarity...
 */
// Register new plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Events',
    'Facebook Events'
);

// Remove unnecessary fields and register plugin flexform
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY . '_events'] = 'select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_events'] = 'pi_flexform';

// Add flexform file reference
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $_EXTKEY . '_events',
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/events.xml'
);

/*
 * PLUGIN FEED
 * Function is just for clarity...
 */

// Register new plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Feed',
    'Facebook Posts Feed'
);

// Remove unnecessary fields and register plugin flexform
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY . '_feed'] = 'select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_feed'] = 'pi_flexform';

// Add flexform file reference
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $_EXTKEY . '_feed',
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/feed.xml'
);

/*
 * DATABASE FIELDS CONFIGURATION REFERENCE
 */

/*
 * Model Feed
 */
$TCA['tx_fbfeed_domain_model_feed'] = array(
    'ctrl' => array(
        'title' => 'Fbfeed',
        'label' => 'name'
    ),
    'columns' => array(
        'content_element_id' => array(),
        'page_id' => array(
            'label' => 'Facebook Page Id',
            'config' => array(
                'type' => 'input'
            )
        ),
        'name' => array(
            'label' => 'Feed Name',
            'config' => array(
                'type' => 'input',
                'size' => '20',
                'eval' => 'trim',
                'max' => '255'
            )
        ),
        'max_posts' => array(
            'label' => 'Max Posts shown',
            'config' => array(
                'type' => 'input',
                'size' => '4',
                'eval' => 'int'
            )
        ),
        'post_template' => array(
            'label' => 'Post Template',
            'config' => array()
        ),
        'max_characters' => array(
            'label' => 'Maximal number of characters',
            'config' => array()
        ),
        'likes' => array(
            'label' => 'Likes',
            'config' => array()
        ),
        'reactions' => array(
            'label' => 'Reactions',
            'config' => array()
        ),
        'shares' => array(
            'label' => 'Shares',
            'config' => array()
        ),
        'comments' => array(
            'label' => 'Comments',
            'config' => array()
        ),
        'images' => array(
            'label' => 'Images',
            'config' => array()
        ),
        'videos' => array(
            'label' => 'Videos',
            'config' => array()
        ),
        'events' => array(
            'label' => 'Events',
            'config' => array()
        ),
        'status' => array(
            'label' => 'Status',
            'config' => array()
        ),
        'links' => array(
            'label' => 'Links',
            'config' => array()
        )
    )
);

/*
 * Model Feed
 */
$TCA['tx_fbfeed_domain_model_eventsoverview'] = array(
    'columns' => array(
        'content_element_id' => array(),
        'page_id' => array(),
        'name' => array(),
        'max_events' => array(),
        'events_state' => array(),
        'show_images' => array(),
    )
);

/*
 * Model Page
 */
$TCA['tx_fbfeed_domain_model_page'] = array(
    'columns' => array(
        'page_id' => array(),
        'id' => array(),
        'name' => array(),
        'about' => array(),
        'picture' => array(),
        'cover' => array(),
        'fan_count' => array()
    )
);

/*
 * Model Event
 */
$TCA['tx_fbfeed_domain_model_event'] = array(
    'columns' => array(
        'page_id' => array(),
        'id' => array(),
        'name' => array(),
        'description' => array(),
        'place' => array(),
        'cover' => array(),
        'start_time' => array(),
        'end_time' => array(),
        'guest_list_enabled' => array(),
        'can_guests_invite' => array(),
        'attending_count' => array(),
        'declined_count' => array(),
        'interested_count' => array(),
        'maybe_count' => array(),
        'noreply_count' => array()
    )
);

/*
 * Model Post
 */
$TCA['tx_fbfeed_domain_model_post'] = array(
    'columns' => array(
        'page_id' => array(),
        'id' => array(),
        'created_time' => array(),
        'type' => array(),
        'is_hidden' => array(),
        'is_published' => array(),
        'shares' => array(),
        'comments' => array(),
        'like' => array(),
        'love' => array(),
        'haha' => array(),
        'wow' => array(),
        'sad' => array(),
        'angry' => array(),
        'message' => array(),
        'picture' => array(),
        'full_picture' => array(),
        'source' => array(),
        'link' => array(),
        'permalink_url' => array()
    )
);