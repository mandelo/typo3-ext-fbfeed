<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Fbfeed',
    'description' => 'Typo3 extension for a simple facebook posts feed.',
    'category' => 'module',
    'author' => 'Simon Oswald',
    'author_email' => 'simon.oswald@somedia.ch',
    'state' => 'beta',
    'version' => '1.0',
    'constraints' => array(
        'depends' => array(
            'php' => '7.0.22',
            'typo3' => '^8.0'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);